﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField]
    private GameObject _anzuelo;

    [Header("events")]
    [SerializeField]
    private GameEvents.VoidEvent _plopEvent;

    private void Start()
    {
        _anzuelo.SetActive(false);
    }

    public void EnableAnzuelo()
    {
        _anzuelo.SetActive(true);
        _plopEvent?.Raise();
    }

    public void DisableAnzuelo()
    {
        _anzuelo.SetActive(false);
    }
}
