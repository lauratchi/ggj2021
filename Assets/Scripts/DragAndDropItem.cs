using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDropItem : MonoBehaviour
{
    [SerializeField]
    private Material _outlineMaterial;
    [SerializeField]
    private bool _hasDefaultPos = true;

    private SpriteRenderer _renderer;
    private Material _defaultMaterial;
    private int _defaultSortingLayer;
    private Vector3 _initialPos;
    private bool _drag;
    private int _itemsLayerMask;
    private int _dropAreaLayerMask;
    private int _dragSortingLayer;
    private int _dropSortingLayer;

    private static int _itemsDragged = 0;
    private static int _itemsDropped = 0;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _defaultMaterial = _renderer.material;
        _defaultSortingLayer = _renderer.sortingLayerID;
        _initialPos = transform.position;
        _itemsLayerMask = LayerMask.GetMask("Items");
        _dropAreaLayerMask = LayerMask.GetMask("DropArea");
        _dragSortingLayer = SortingLayer.NameToID("Drag");
        _dropSortingLayer = SortingLayer.NameToID("Drop");
    }

    private void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (_drag)
        {

            transform.position = new Vector3(mousePos.x, mousePos.y + 0.5f, 0f);
        }
        else
        {

            Collider2D hit = Physics2D.OverlapPoint(new Vector2(mousePos.x, mousePos.y), _itemsLayerMask);
            if (hit != null && hit.gameObject == gameObject && _itemsDragged == 0)
            {
                _renderer.material = _outlineMaterial;
            }
            else
            {
                _renderer.material = _defaultMaterial;
            }
        }
    }

    public void Drag()
    {
        Debug.Log("--> Drag");
        _drag = true;
        _renderer.material = _outlineMaterial;
        _renderer.sortingLayerID = _dragSortingLayer;
        _itemsDragged++;
    }

    public bool Drop()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D hit = Physics2D.OverlapPoint(new Vector2(mousePos.x, mousePos.y), _dropAreaLayerMask);
        if (hit != null)
        {
            Debug.Log("--> Drop");
            _initialPos = transform.position;
            _drag = false;
            _renderer.material = _defaultMaterial;
            _renderer.sortingLayerID = _dropSortingLayer;
            _renderer.sortingOrder = _itemsDropped;
            _itemsDropped++;
            _itemsDragged--;
            return true;
        }
        else if (_hasDefaultPos)
        {
            Debug.Log("--> Reset");
            transform.position = _initialPos;
            _drag = false;
            _renderer.material = _defaultMaterial;
            _renderer.sortingLayerID = _defaultSortingLayer;
            return true;
        }

        return false;
    }
}
