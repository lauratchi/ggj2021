using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource _startFishing;
    [SerializeField]
    private AudioSource _plop;
    [SerializeField]
    private AudioSource _blop;
    [SerializeField]
    private AudioSource _bigBlop;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void StartFishing()
    {
        _startFishing.Play();
    }

    public void Plop()
    {
        _plop.Play();
    }

    public void OnBlop()
    {
        _blop.Play();
    }

    public void OnBigBlop()
    {
        _bigBlop.Play();
    }
}
