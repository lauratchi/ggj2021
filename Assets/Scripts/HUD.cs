﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUD : MonoBehaviour
{
    [SerializeField]
    private Image _image;
    [SerializeField]
    private TextMeshProUGUI _text;

    [Header("Events")]
    [SerializeField]
    private GameEvents.VoidEvent _transitionInEvent;
    [SerializeField]
    private GameEvents.VoidEvent _transitionOutEvent;

    [Header("Data")]
    [SerializeField]
    private BonesData _bonesData;
    [SerializeField]
    private PescaditosData _pescaditosData;

    private Animator _animator;
    private int _transitionInTriggerHash = Animator.StringToHash("TransitionIn");
    private int _transitionOutTriggerHash = Animator.StringToHash("TransitionOut");

    private bool _enabled;
    private bool _canTransitionOut;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _enabled = false;
        _canTransitionOut = false;
    }

    private void Update()
    {
        if (_enabled && Input.GetMouseButtonDown(0))
        {
            OnTransitionOut();
        }
    }

    public void OnTransitionIn(int boneIndex)
    {
        _text.text = "";

        if (boneIndex >= 0)
        {
            _image.sprite = _bonesData.bones[boneIndex]._sprite;

            StartCoroutine(TypeText(_bonesData.bones[boneIndex].Text));
        }
        else
        {
            int randomIndex = Random.Range(0, _pescaditosData.pescaditos.Count);
            _image.sprite = _pescaditosData.pescaditos[randomIndex]._sprite;

            StartCoroutine(TypeText(_pescaditosData.pescaditos[randomIndex].Text));
        }

        _animator.SetTrigger(_transitionInTriggerHash);
        _enabled = true;

        _transitionInEvent?.Raise();
    }

    private IEnumerator TypeText(string text)
    {
        foreach (char letter in text.ToCharArray())
        {
            _text.text += letter;
            yield return new WaitForSeconds(0.02f);
        }

        _canTransitionOut = true;
    }

    public void OnTransitionOut()
    {
        if (_canTransitionOut)
        {
            _canTransitionOut = false;

            _animator.SetTrigger(_transitionOutTriggerHash);
            _enabled = false;

            _transitionOutEvent?.Raise();
        }
    }

}
