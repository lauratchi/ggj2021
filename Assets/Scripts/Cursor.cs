using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    private enum State
    {
        Open,
        Closed,
        Animated,
        Mouse,
        MouseAnimated
    }

    private State _state;
    private Animator _animator;
    private int _openTriggerHash = Animator.StringToHash("Open");
    private int _closeTriggerHash = Animator.StringToHash("Close");
    private int _grabbingTriggerHash = Animator.StringToHash("Grabbing");
    private int _mouseTriggerHash = Animator.StringToHash("Mouse");
    private int _clickTriggerHash = Animator.StringToHash("Click");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        Open();
    }

    private void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
    }

    public void TryOpen()
    {
        if (!IsOpen)
        {
            Open();
        }
    }

    public void TryClose()
    {
        if (!IsClosed)
        {
            Close();
        }
    }

    public void TryAnimate()
    {
        if (!IsAnimated)
        {
            Animate();
        }
    }

    public void TryMouse()
    {
        if (!IsMouse)
        {
            Mouse();
        }
    }

    public void TryMouseAnimate()
    {
        if (!IsMouseAnimated)
        {
            MouseAnimate();
        }
    }

    private void Open()
    {
        _state = State.Open;
        _animator.SetTrigger(_openTriggerHash);
    }

    private void Close()
    {
        _state = State.Closed;
        _animator.SetTrigger(_closeTriggerHash);
    }

    private void Animate()
    {
        _state = State.Animated;
        _animator.SetTrigger(_grabbingTriggerHash);
    }

    private void Mouse()
    {
        _state = State.Mouse;
        _animator.SetTrigger(_mouseTriggerHash);
    }

    private void MouseAnimate()
    {
        _state = State.MouseAnimated;
        _animator.SetTrigger(_clickTriggerHash);
    }

    public bool IsOpen { get { return _state == State.Open; } }
    public bool IsClosed { get { return _state == State.Closed; } }
    public bool IsAnimated { get { return _state == State.Animated; } }
    public bool IsMouse { get { return _state == State.Mouse; } }
    public bool IsMouseAnimated { get { return _state == State.MouseAnimated; } }
}
