using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedEvent : MonoBehaviour
{
    private Animation _animation;

    private void Awake()
    {
        _animation = GetComponent<Animation>();
    }

    public void Animate()
    {
        if (!_animation.isPlaying)
        {
            _animation.Play();
        }
    }
}
