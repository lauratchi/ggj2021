﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jarroncitos : MonoBehaviour
{
    private Animator _animator;

    private int _pescaditosHash = Animator.StringToHash("Pescaditos");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void OnPescadito(int pescaditos)
    {
        _animator.SetInteger(_pescaditosHash, pescaditos);
    }
}
