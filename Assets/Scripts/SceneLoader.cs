﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    // serialized data

    [SerializeField]
    private Animator transition;
    [SerializeField]
    private float transitionTime = 1f;

    [Space(10)]
    [SerializeField]
    private bool restart = false;

    // public methods

    public void LoadNextScene()
    {
        if (restart)
        {
            StartCoroutine(LoadScene(0));
        }
        else
        {
            StartCoroutine(LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
        }
    }

    // private methods

    private IEnumerator LoadScene(int sceneIdx)
    {
        transition.SetTrigger("start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(sceneIdx);
    }
}
