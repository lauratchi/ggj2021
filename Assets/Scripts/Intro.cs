﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    [SerializeField]
    private GameEvents.VoidEvent _languageSelectedEvent;

    public void SelectLanguage(int languageIndex)
    {
        PlayerPrefs.SetInt("language_index", languageIndex);

        _languageSelectedEvent?.Raise();
    }
}
