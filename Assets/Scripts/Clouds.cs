﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour
{
    [System.Serializable]
    public class CloudsLayer
    {
        public Transform Layer;
        public Vector3 LeftPos;
        public Vector3 RightPos;
        public float Time;
        public float Start;

        private float _currentTime;
        public void SetStart()
        {
            _currentTime = Start * Time;
        }

        public void Move(float deltaTime)
        {
            if (_currentTime >= Time)
            {
                _currentTime = 0f;
            }

            Layer.localPosition = Vector3.Lerp(LeftPos, RightPos, _currentTime / Time);

            _currentTime += deltaTime;
        }
    }

    [SerializeField]
    private CloudsLayer[] _layers;

    private void Start()
    {
        foreach(var layer in _layers)
        {
            layer.SetStart();
        }
    }

    private void Update()
    {
        foreach(var layer in _layers)
        {
            layer.Move(Time.deltaTime);
        }
    }
}
