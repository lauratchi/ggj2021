﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PescaditoData
{
    public Sprite _sprite;
    public string _english;
    public string _spanish;
    public string _catalan;

    public string Text
    {
        get
        {
            int languageIndex = PlayerPrefs.GetInt("language_index", 0);
            switch (languageIndex)
            {
                case 0: return _english;
                case 1: return _spanish;
                case 2: return _catalan;
                default: return _english;
            }
        }
    }
}

[CreateAssetMenu(fileName = "PescaditosData", menuName = "ScriptableObjects/PescaditosData")]
public class PescaditosData : ScriptableObject
{
    public List<PescaditoData> pescaditos;
}
