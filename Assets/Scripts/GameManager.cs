using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private enum State
    {
        Idle,
        Waiting,
        PreFishing,
        Fishing
    }

    [SerializeField]
    private Cursor _cursor;

    [Header("Fishing Values")]
    [SerializeField]
    private float _minWaitTime = 3f;
    [SerializeField]
    private float _maxWaitTime = 10f;

    [SerializeField]
    private int _minFakeFishes = 0;
    [SerializeField]
    private int _maxFakeFishes = 5;
    [SerializeField]
    private float _minFakeFishTime = 1f;
    [SerializeField]
    private float _maxFakeFishTime = 1.5f;

    [SerializeField]
    private float _maxFishingAttemptTime = 1f;

    [Header("Events")]
    [SerializeField]
    private GameEvents.VoidEvent _idleEvent;
    [SerializeField]
    private GameEvents.VoidEvent _waitEvent;
    [SerializeField]
    private GameEvents.VoidEvent _blopEvent;
    [SerializeField]
    private GameEvents.VoidEvent _bigBlopEvent;
    [SerializeField]
    private GameEvents.IntEvent _successsEvent;
    [SerializeField]
    private GameEvents.IntEvent _pescaditoEvent;

    [Header("Data")]
    [SerializeField]
    private BonesData _bonesData;
    [SerializeField]
    private Transform _bonesParent;

    private State _state;

    private DragAndDropItem _item;
    private int _itemsLayerMask;
    private int _characterLayerMask;

    private int _currentBones;
    private int _currentPescaditos;

    private bool _inputEnabled;

    private const int MAX_PESCADITOS = 8;

    private void Start()
    {
        Init();
    }

    private void Update()
    {
        switch(_state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Waiting:
                Waiting();
                break;
            case State.PreFishing:
                PreFishing();
                break;
            case State.Fishing:
                Fishing();
                break;
        }
    }

    public void EnableInput()
    {
        _inputEnabled = true;
    }

    public void DisableInput()
    {
        _inputEnabled = false;
    }

    private void Init()
    {
        _currentBones = 0;
        _currentPescaditos = 0;
        _item = null;
        _itemsLayerMask = LayerMask.GetMask("Items");
        _characterLayerMask = LayerMask.GetMask("Character");
        _inputEnabled = true;
        StartIdle();
    }

    private void StartIdle()
    {
        Debug.Log(">> IDLE");

        _idleEvent?.Raise();
        _state = State.Idle;
    }

    private void Idle()
    {
        if (!_inputEnabled) return;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            // Miramos si hemos clickado en un objeto
            Collider2D itemHit = Physics2D.OverlapPoint(new Vector2(mousePos.x, mousePos.y), _itemsLayerMask);
            if (itemHit != null)
            {
                _item = itemHit.GetComponent<DragAndDropItem>();
                _item?.Drag();
                _cursor?.TryClose();
            }
            else
            {
                // Miramos si hemos clickado en el personaje
                Collider2D characterHit = Physics2D.OverlapPoint(new Vector2(mousePos.x, mousePos.y), _characterLayerMask);
                if (characterHit != null)
                {
                    FinishIdle();
                    StartWaiting();
                }
                else
                {
                    _cursor?.TryClose();
                }
            }
        }
        else if (Input.GetMouseButton(0))
        {
            _cursor?.TryClose();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (_item != null)
            {
                if (_item.Drop())
                {
                    _item = null;
                }
                _cursor?.TryOpen();
            }
        }
        else
        {
            if (_item == null) // Esto por el caso en el que dejamos el hueso en la mano
            {
                // Si no estamos clickando miramos si estamos encima de un objeto
                Collider2D itemHit = Physics2D.OverlapPoint(new Vector2(mousePos.x, mousePos.y), _itemsLayerMask);
                if (itemHit != null)
                {
                    _cursor?.TryAnimate();
                }
                else // Si no miramos si estamos encima del personaje
                {
                    Collider2D characterHit = Physics2D.OverlapPoint(new Vector2(mousePos.x, mousePos.y), _characterLayerMask);
                    if (characterHit != null)
                    {
                        _cursor?.TryMouse();
                    }
                    else // Y si no estamos encima de nada pues dejamos la mano abierta
                    {
                        _cursor?.TryOpen();
                    }
                }
            }
        }
    }

    private void FinishIdle()
    {
        _item?.Drop();
        _cursor?.TryMouse();
    }

    // WAITING

    private float _currentWaitTime;
    private float _targetWaitTime;

    private void StartWaiting()
    {
        Debug.Log(">> WAITING");

        _waitEvent?.Raise();
        _state = State.Waiting;
        _currentWaitTime = 0f;
        _targetWaitTime = Random.Range(_minWaitTime, _maxWaitTime);

        // Tirar la cania animation...
    }

    private void Waiting()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log(">> FAILED");

            StartIdle();
            return;
        }

        if (_currentWaitTime >= _targetWaitTime)
        {
            StartPreFishing();
        }
        // else do waiting stuff ?

        _currentWaitTime += Time.deltaTime;
    }

    // PRE-FISHING

    private int _currentNumFakeFishes;
    private int _targetNumFakeFishes;

    private float _currentFakeFishTime;
    private float _targetFakeFishTime;

    private void StartPreFishing()
    {
        Debug.Log(">> PRE-FISHING");

        _state = State.PreFishing;
        _currentNumFakeFishes = 0;
        _targetNumFakeFishes = Random.Range(_minFakeFishes, _maxFakeFishes + 1);

        StartFakeFish();
    }

    private void StartFakeFish()
    {
        _currentFakeFishTime = 0f;
        _targetFakeFishTime = Random.Range(_minFakeFishTime, _maxFakeFishTime);
    }

    private void FinishFakeFish()
    {
        Debug.Log(">> BLOP!");

        _blopEvent?.Raise();

        StartFakeFish();
    }

    private void PreFishing()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log(">> FAILED");

            StartIdle();
            return;
        }

        if (_currentFakeFishTime >= _targetFakeFishTime)
        {
            _currentNumFakeFishes++;

            if (_currentNumFakeFishes > _targetNumFakeFishes)
            {
                StartFishing();
            }
            else
            {
                FinishFakeFish();
            }
        }
        // else do waiting stuff ?

        _currentFakeFishTime += Time.deltaTime;
    }

    // FISHING

    private float _currentFishingTime;

    private void StartFishing()
    {
        Debug.Log(">> FISHING");

        _state = State.Fishing;
        _currentFishingTime = 0f;

        _bigBlopEvent?.Raise();

        _cursor?.TryMouseAnimate();
    }

    private void Fishing()
    {
        if (_currentFishingTime <= _maxFishingAttemptTime)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log(">> SUCCESS");

                Success();
            }
        }
        else
        {
            Debug.Log(">> FAILED");

            StartIdle();
        }

        _currentFishingTime += Time.deltaTime;
    }

    // SUCCESS!

    private void Success()
    {
        // Add feedback...
        DisableInput();
        StartIdle();

        bool isBone = Random.Range(0, 2) == 0 && _currentBones < _bonesData.bones.Count;

        if (isBone)
        {
            _successsEvent?.Raise(_currentBones);
            DragAndDropItem bone = Instantiate(_bonesData.bones[_currentBones]._item, _bonesParent);
            bone.Drag();
            _item = bone;
            _cursor?.TryClose();

            _currentBones++;
        }
        else
        {
            _cursor?.TryOpen();
            _successsEvent?.Raise(-1);

            if (_currentPescaditos <= MAX_PESCADITOS)
            {
                _currentPescaditos++;
                _pescaditoEvent?.Raise(_currentPescaditos);
            }
        }
    }
}
