﻿using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    [System.Serializable]
    public class Vector3Response : UnityEvent<Vector3> { }
}
