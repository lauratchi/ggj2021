﻿using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    [System.Serializable]
    public class IntResponse : UnityEvent<int> { }
}
